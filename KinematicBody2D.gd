extends KinematicBody2D

signal hit

export (float) var bullet_delay = 0.5 
var waited  = 0
export (float) var bullet_angle = 0
export (int) var bullet_speed = 8
var directional_force = Vector2()
export (PackedScene) var Bullet_scene

export (int) var bullet_gravity = 0
export (NodePath) var bullet_spawn_path
onready var bullet_spawn = get_node(bullet_spawn_path)
var shooting = false

export var gravity = 2500
export var speed = 300
export var jump = -900
var jumping = false
var velocity = Vector2()


func get_input():
	velocity.x = 0
	jumping = false
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
	if Input.is_action_pressed("ui_left"):
		velocity.x -=1
	velocity.x *= speed
	if Input.is_action_just_pressed("ui_up"):
		jumping = true


	$AnimatedSprite.play()

	if velocity.x != 0:
		$AnimatedSprite.animation = "right"
		$AnimatedSprite.flip_v = false
		$AnimatedSprite.flip_h = velocity.x < 0
	elif velocity.y != 1:
		$AnimatedSprite.animation = "down"
		velocity.y > 0
	else:
		$AnimatedSprite.animation = "up"
		

func _on_Player_body_entered(body):

	emit_signal("hit")
	
	
func _physics_process(delta):
	velocity.y += gravity * delta
	get_input()
	velocity = move_and_slide(velocity, Vector2(0, -1))
	for l in get_slide_count():
		var collision = get_slide_collision(l)
		if collision:
			emit_signal('hit', collision)
			
	if is_on_floor() and jumping:
		velocity.y = jump
		



func update_directional_force():
	directional_force = Vector2() * bullet_speed

func shoot():
	var bullet = Bullet_scene.instance()
	bullet.set_global_position(bullet_spawn.get_global_position())
	bullet.shoot(directional_force, bullet_gravity)
	get_parent().add_child(bullet)

func _ready():
	update_directional_force()
	set_process_input(true)
	set_process(true)
	
func _input(event):
	if(event.is_action_pressed("ui_select")):
		shooting = true
	elif(event.is_action_released("ui_select")):
		shooting = false

func _process(delta):
	if (shooting && waited > bullet_delay):
		fire_once()
		waited = 0 
	elif(waited <= bullet_delay):
		waited =+ delta

func fire_once():
	shoot()
	shooting = false







