extends KinematicBody2D


const BULLET = preload("res://Bullet.tscn")


export var gravity = 2500
export var speed = 300
export var jump = -900
var jumping = false
var velocity = Vector2()

func get_input():
	velocity.x = 0
	jumping = false
	if Input.is_key_pressed (KEY_D):
		velocity.x += 1
	if Input.is_key_pressed (KEY_A):
		velocity.x -=1
	velocity.x *= speed
	if Input.is_key_pressed (KEY_W):
		jumping = true
		

	$AnimatedSprite.play()

	if velocity.x != 0:
		$AnimatedSprite.animation = "right"
		$AnimatedSprite.flip_v = false
		$AnimatedSprite.flip_h = velocity.x < 0
	elif velocity.y != 1:
		$AnimatedSprite.animation = "down"

	else:
		$AnimatedSprite.animation = "up"
		

func _physics_process(delta):
	velocity.y += gravity * delta
	get_input()
	velocity = move_and_slide(velocity, Vector2(0, -1))
			
	if is_on_floor() and jumping:
		velocity.y = jump
		
	if Input.is_key_pressed (KEY_C): 
		var bullet = BULLET.instance()
		get_parent().add_child(bullet)
		bullet.position = $Position2D.global_position

