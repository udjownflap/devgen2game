extends Area2D

var SPEED = 700
var velocity = Vector2()
var _flip = 0


func _ready():
	pass
	

func _physics_process(delta):
	velocity.x = SPEED * delta
	
	
	var OB = get_overlapping_bodies()
	if OB:
		print("Overlapping: ")
		print(OB)
		
	
	translate(velocity)
	$AnimatedSprite.play("shoot")
	if _flip:
		$AnimatedSprite.flip_h = true
	

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

func _on_Bullet_body_entered(body):
	if body.name == "Player":
		queue_free()
		get_tree().change_scene("res://FirstLevel.tscn")
		
	elif body.name == "Player2":
		queue_free()
		get_tree().change_scene("res://FirstLevel.tscn")
	elif body.name == "TileMap":
		queue_free()