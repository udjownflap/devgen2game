





extends KinematicBody2D



const BULLET = preload("res://Bullet.tscn")

export var gravity = 2500
export var speed = 300
export var jump = -900
var jumping = false
var velocity = Vector2()


func get_input():
	velocity.x = 0
	jumping = false
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
	if Input.is_action_pressed("ui_left"):
		velocity.x -=1
	velocity.x *= speed
	if Input.is_action_just_pressed("ui_up"):
		jumping = true


	$AnimatedSprite.play()

	if velocity.x != 0:
		$AnimatedSprite.animation = "right"
		$AnimatedSprite.flip_v = false
		$AnimatedSprite.flip_h = velocity.x < 0

	elif velocity.y != 1:
		$AnimatedSprite.animation = "down"
		
	else:
		$AnimatedSprite.animation = "up"
		

export (NodePath) var bullet_spawn_path
onready var bullet_spawn = get_node(bullet_spawn_path)
	
func _physics_process(delta):
	velocity.y += gravity * delta
	get_input()
	velocity = move_and_slide(velocity, Vector2(0, -1))
			
	if is_on_floor() and jumping:
		velocity.y = jump
		
	if Input.is_action_just_pressed("ui_select"):
		spawn_bullet()
	
	
	
func spawn_bullet():
	var bullet = BULLET.instance()
	get_parent().add_child(bullet)
	bullet.set_position(get_node("Position2D").get_global_position())
	if $AnimatedSprite.flip_h:
		BULLET.instance()
		get_parent().add_child(bullet)
		bullet.set_position(get_node("Position2D2").get_global_position())
		bullet.SPEED = bullet.SPEED*-1
		bullet._flip = 1
		


















		



