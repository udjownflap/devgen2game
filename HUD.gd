extends CanvasLayer

signal start_game
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func show_message(text):
	$GameStart.text = text
	$GameStart.show()
	
	
	

func _on_start():
	emit_signal("start_game")
	$GameStart.hide()
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


